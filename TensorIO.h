/*
   TensorIO.h
 
   Created on: Jun 22, 2011
       Author: Christoph Jud
        Email: christoph.jud@unibas.ch


Copyright (c) 2011, Christoph Jud
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#ifndef __TensorIO_h__
#define __TensorIO_h__

#include <iostream>
#include <fstream>
#include <string>

#include <vnl/vnl_matlab_read.h>
#include <vnl/vnl_matlab_write.h>
#include <vnl/vnl_matrix.h>

/** reads a tensor from a matlab mat file
 * 	- tensor is assumed to be stored in the mode-1 unfolding
 */
vnl_matrix<double> read_tensor(std::string filename){
        std::ifstream matStream;
        matStream.open (filename.c_str());

        if (!matStream) std::cerr << "Error while opening " << filename << " for reading." << std::endl;

        vnl_matrix<double> m;

        if(vnl_matlab_read_or_die(matStream, m, "F")){
                std::cerr << "Tensor read successful." << std::endl;
        }
        else{
                std::cerr << "Error while reading tensor." << std::endl;
        }

        matStream.close();

        return m;
}

/** writes a tensor to matlab mat format
 * 	- tensor is stored in the mode-1 unfolding
 */
bool write_tensor(std::string filename, vnl_matrix<double> F){
		std::ofstream matStream;
		matStream.open(filename.c_str());

		if(!matStream) std::cerr << "Error while opening " << filename << " for writing." << std::endl;

		bool success = vnl_matlab_write<double>(matStream, F.data_array(), F.rows(), F.cols(), "F");

		matStream.close();

		if( success )
			std::cerr << "Tensor written successful." << std::endl;
		else
			std::cerr << "Error while writing tensor." << std::endl;

		return success;
}

#endif
