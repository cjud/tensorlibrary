/*
   Tensor.cpp
 
   Created on: Jun 22, 2011
       Author: Christoph Jud
        Email: christoph.jud@unibas.ch


Copyright (c) 2011, Christoph Jud
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include "Tensor.h"
#include <assert.h>
#include <cmath>
#include <limits>
#include <sys/time.h>

#include <vnl/vnl_vector.h>
#include <vnl/algo/vnl_svd.h>
#include <vnl/vnl_inverse.h>
#include <vnl/algo/vnl_sparse_symmetric_eigensystem.h>
#include <vnl/algo/vnl_svd_economy.h>

Tensor::MatrixPointer Tensor::unfolding(unsigned mode)const{
	MatrixPointer mrx ( new Matrix() );

	Tensor::const_iterator it;
	unsigned counter;
	unsigned c = this->num_columns();
	unsigned r = this->num_rows();
	unsigned t = this->num_tubes();

	switch(mode){
	case 1: // mode-1 (column) fibers: x_:jk
		mrx->set_size(r,c*t);
		for(it=this->begin(), counter=0; it!=this->end(); it++, counter++){
			mrx->operator()( (counter/c) % r, ((counter/(c*r))*c)+(counter%c) ) = *it;
		}
		break;
	case 2: // mode-2 (row) fibers: x_i:k
		mrx->set_size(c,r*t);
		for(it=this->begin(), counter=0; it!=this->end(); it++, counter++){
			mrx->operator()(counter%c, counter/c) = *it;
		}
		break;
	case 3: // mode-3 (tube) fibers: x_ik:
		mrx->set_size(t,r*c);
		for(it=this->begin(), counter=0; it!=this->end(); it++, counter++){
			mrx->operator()(counter/(r*c), (counter%(r*c)*r)%(r*c) + (counter/c)%r) = *it;
		}
		break;
	default:
		std::cerr << "Error: only 3-mode tensors are supported." << std::endl;
	}
	return mrx;
}

Tensor::TensorPointer Tensor::folding(Matrix &mrx, unsigned mode, unsigned c, unsigned r, unsigned t){
	TensorPointer T ( new Tensor(c,r,t) );
	Tensor::iterator it;
	unsigned counter;

	switch(mode){
	case 1:
		assert(mrx.rows() == r && mrx.columns() == c*t);
		for(it=T->begin(), counter=0; it!=T->end(); it++, counter++){
			*it = mrx( (counter/c) % r, ((counter/(c*r))*c)+(counter%c) );
		}
		break;
	case 2:
		assert(mrx.rows() == c && mrx.columns() == r*t);
		for(it=T->begin(), counter=0; it!=T->end(); it++, counter++){
			*it = mrx(counter%c, counter/c);
		}
		break;
	case 3:
		assert(mrx.rows() == t && mrx.columns() == c*r);
		for(it=T->begin(), counter=0; it!=T->end(); it++, counter++){
			*it = mrx(counter/(r*c), (counter%(r*c)*r)%(r*c) + (counter/c)%r);
		}
		break;
	default:
		std::cerr << "Error: only 3-mode tensors are supported." << std::endl;
	}
	return T;
}


/** calculate the sum of squared error of this Tensor and the Tensor T */
double Tensor::sse(const TensorPointer T)const{
	double error = 0;
	Tensor::const_iterator ita, itb;
	for(ita=this->begin(), itb=T->begin(); ita!=this->end() && itb!=T->end(); ita++, itb++){
		error += pow(fabs(*ita)-fabs(*itb),2);
	}
	return error;
}

/** returns: [0 A; A' 0] */
Tensor::SparseMatrixPointer Tensor::GetSymmetricSparseMatrix(const Matrix &A){
	unsigned num_lines = A.rows();
	unsigned num_columns = A.columns();
	unsigned n = num_lines+num_columns;

	SparseMatrixPointer B ( new SparseMatrix(n,n) );
	for(unsigned i=0; i<num_lines; i++){
		for(unsigned j=0; j<num_columns; j++){
			(*B)(i,num_lines+j) = A(i,j);
			(*B)(num_lines+j,i) = A(i,j);
		}
	}
	return B;
}

/** returns: [0 A; A' 0] */
Tensor::MatrixPointer Tensor::GetSymmetricMatrix(const Matrix &A){
	unsigned num_lines = A.rows();
	unsigned num_columns = A.columns();
	unsigned n = num_lines+num_columns;

	MatrixPointer B ( new Matrix(n,n,0) );
	for(unsigned i=0; i<num_lines; i++){
		for(unsigned j=0; j<num_columns; j++){
			(*B)(i,num_lines+j) = A(i,j);
			(*B)(num_lines+j,i) = A(i,j);
		}
	}
	return B;
}

Tensor::VectorPointer Tensor::Matrix2Vector(const Matrix &A){
	VectorPointer B ( new Vector(A.size()) );
	Matrix::const_iterator m_it = A.begin();
	for(Vector::iterator v_it = B->begin(); v_it!=B->end(); v_it++){
		*v_it = *m_it;
		m_it++;
	}
	return B;
}

Tensor::MatrixPointer Tensor::Vector2Matrix(const Vector &A, unsigned c, unsigned r){
	assert(A.size()==c*r);
	MatrixPointer B ( new Matrix(r,c) );
	Vector::const_iterator v_it = A.begin();
	for(Matrix::iterator m_it = B->begin(); m_it!=B->end(); m_it++){
		*m_it = *v_it;
		v_it++;
	}
	return B;
}

void Tensor::set_size(unsigned c, unsigned r, unsigned t){
	delete [] data;
	data = new double[c*r*t];
}

void Tensor::set_size(unsigned c, unsigned r, unsigned t, double value){
	this->set_size(c,r,t);
	this->fill(value);
}

/** Higher Order Singular Value Decomposition
 *
 *  Computes the higher order SVD of the Tensor.
 *
 *  Input: unsigned r 			- how many ranks have to be calculated
 *  Output: Matrix core 		- mode-1 unfolded full tensor
 *  		vector of Matrix 	- contains the factor matrices U_i
 *
 *  This is implemented on the basis of:
 * 	Lieven De Lathauwer, Bart De Moor, and Joos Vandewalle. A
 * 	multilinear singular value decomposition. SIAM journal on Matrix
 * 	Analysis and Applications, 21(4):1253–1278, 2000.
 */
std::vector< Tensor::Matrix > * Tensor::HOSVD(Matrix& core, unsigned r)const{
	std::vector< Matrix >* factor_matrices = new std::vector< Matrix >();
	for(unsigned i=1; i<=3; i++){
		MatrixPointer t_unfolded = this->unfolding(i);
		vnl_svd_economy<double> svd((*t_unfolded).transpose());
		factor_matrices->push_back(svd.V().get_n_columns(0,r));
	}

	MatrixPointer kron_product = kron(factor_matrices->at(1),factor_matrices->at(2));
	MatrixPointer t_unfolded = this->unfolding(1);
	core = factor_matrices->at(0).transpose() * (*t_unfolded) * (*kron_product);
	return factor_matrices;
}


/** CANDECOMP / PARAFAC Decomposition
 *
 *  Computes a CP tensor decomposition
 *
 *  This is implemented on the basis of:
 * 	Tamara G Kolda and Brett W Bader. Tensor decompositions and
 * 	applications. SIAM review, 51(3):455–500, 2009.
 */
std::vector< Tensor::Matrix > * Tensor::CPALS(Vector& lambdas, unsigned r, unsigned num_iter, double tol, bool print_iter)const{

	/** initialisation with principal eigenvectors of T_i * T_i' */
	Matrix core;
	std::vector< Matrix >* factor_matrices = this->HOSVD(core, r);

	Matrix A = factor_matrices->at(0);
	Matrix B = factor_matrices->at(1);
	Matrix C;

	lambdas.set_size(r);

	MatrixPointer X1 = this->unfolding(1);
	MatrixPointer X2 = this->unfolding(2);
	MatrixPointer X3 = this->unfolding(3);

	/** main loop */
	for(unsigned i=0; i<num_iter; i++){
		/** perform als */
		C = Tensor::als(*X3, A, B);
		B = Tensor::als(*X2, A, C);
		A = Tensor::als(*X1, B, C, false);

		for(unsigned n=0; n<r; n++)
			lambdas[n] = A.get_column(n).two_norm();

		A.normalize_columns();

		/** evaluate error */
		factor_matrices->clear();
		factor_matrices->push_back(A);
		factor_matrices->push_back(B);
		factor_matrices->push_back(C);

		TensorPointer T = Tensor::GetTensor(factor_matrices, lambdas);
		double error = this->sse(T);
		if(error<tol)
			break;

		if(print_iter) std::cout << "Iteration: " << i << ", error: " << error << std::endl;
	}

	return factor_matrices;
}

/** Tucker Tensor
 *	- core has to be a mode-1 unfolded cubical tensor [cubical: num(rows)==num(columns)==num(tubes)]
 *	- factor_matrices is a standard vector of size 3 containing the factor matrices U_i
 */
Tensor::TensorPointer Tensor::GetTensor(std::vector< Matrix >* factor_matrices, Matrix& core){
	std::cout << "factor_size: " << factor_matrices->size() << std::endl;
	std::cout << "core size: " << core.size() << std::endl;
	assert(factor_matrices->size() == 3);
	std::cout << "performing kron product" << std::endl;
	MatrixPointer kron_product = Tensor::kron(factor_matrices->at(1),factor_matrices->at(2));
	std::cout << "perform multiplications" << std::endl;

	std::cout << "factor_matrices->at(0) rows, columns: " << factor_matrices->at(0).rows() << ", " << factor_matrices->at(0).columns() << std::endl;
	std::cout << "(*kron_product).transpose() rows, columns: " << (*kron_product).transpose().rows() << ", " << (*kron_product).transpose().columns() << std::endl;
	std::cout << "core rows columns: " << core.rows() << ", " << core.columns() << std::endl;

	Matrix mode1_unfolded = factor_matrices->at(0) * core * (*kron_product).transpose();
	std::cout << "model1_unfolded rows, columns: "<< mode1_unfolded.rows() << ", " << mode1_unfolded.columns() << std::endl;

	unsigned tensor_dim = factor_matrices->at(0).rows();
	std::cout << "tensor dimensionality: " << tensor_dim << std::endl;
	return Tensor::folding(mode1_unfolded, 1, tensor_dim, tensor_dim, tensor_dim);
}

/** Kruskal Tensor
 * 	- lambdas is a vector containing the superdiagonal elements of the core
 *	- factor_matrices is a standard vector of size 3 containing the factor matrices U_i
 */
Tensor::TensorPointer Tensor::GetTensor(std::vector< Matrix >* factor_matrices, Vector& lambdas){
	assert(factor_matrices->size() == 3);
	DiagMatrix core(lambdas);
	MatrixPointer krb_product = Tensor::krb(factor_matrices->at(1),factor_matrices->at(2));
	Matrix mode1_unfolded = factor_matrices->at(0) * core * (*krb_product).transpose();

	unsigned tensor_dim = factor_matrices->at(0).rows();
	return Tensor::folding(mode1_unfolded, 1, tensor_dim, tensor_dim, tensor_dim);
}

/** Kronecker product
 *  - input A of dim IxJ and B of dim KxL then the output is IK x JL
 *  - MATLAB notation: kron(A,B)
 */
Tensor::MatrixPointer Tensor::kron(Matrix &A, Matrix &B){
	MatrixPointer Y ( new Matrix(A.rows()*B.rows(), A.columns()*B.columns()) );

	for(unsigned i=0; i<A.rows(); i++){
		for(unsigned j=0; j<A.columns(); j++){
			Y->update(A(i,j)*B, i*B.rows(), j*B.columns());
		}
	}
	return Y;
}

/** Khatri-Rao product.
 *  - input A of dim IxK and B of dim JxK then the output is IJ x K
 *  - MATLAB notation krb(A,B)
 */
Tensor::MatrixPointer Tensor::krb(Matrix &A, Matrix &B){
	assert(A.columns()==B.columns());
	MatrixPointer Y ( new Matrix(A.rows()*B.rows(), A.columns()) );

	for(unsigned i=0; i<A.columns(); i++){
		Matrix ab = outer_product(A.get_column(i),B.get_column(i));
		Y->set_column(i,ab.begin());
	}
	return Y;
}

/** Hadamard product (element-wise)
 *  - input A, B and output Y have same dimensions
 *  - MATLAB notation: A.*B
 */
Tensor::MatrixPointer Tensor::hm(Matrix &A, Matrix &B){
	assert(A.columns()==B.columns() && A.rows()==B.rows());
	MatrixPointer Y ( new Matrix(A.rows(),A.columns()) );

	Matrix::const_iterator itA = A.begin(), itB = B.begin();
	Matrix::iterator itY = Y->begin();

	for(; itY!=Y->end(); itA++, itB++, itY++){
		*itY = (*itA) * (*itB);
	}

	return Y;
}

/** Alternating Least Squares
 *
 *   This is implemented on the basis of:
 *	Richard A Harshman. Foundations of the parafac procedure: mod-
 *	els and conditions for an" explanatory" multimodal factor analysis.
 *	1970.
*/
Tensor::Matrix Tensor::als(Matrix &X, Matrix &A, Matrix &B, bool normalize){
	MatrixPointer krb_product = Tensor::krb(B,A);
	Matrix Aproduct = A.transpose()*A;
	Matrix Bproduct = B.transpose()*B;

	MatrixPointer hm_product = Tensor::hm(Aproduct, Bproduct);
	InverseMatrix inv_matrix(*hm_product);

	Matrix C(X * (*krb_product) * inv_matrix.pinverse());
	if(normalize) C.normalize_columns();

	return C;
}











