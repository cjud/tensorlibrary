/*
   Tensor.h
 
   Created on: Jun 22, 2011
       Author: Christoph Jud
        Email: christoph.jud@unibas.ch


Copyright (c) 2011, Christoph Jud
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#ifndef __Tensor_h__
#define __Tensor_h__

#include <iostream>
#include <vector>
#include <memory>

#include <vnl/vnl_matrix.h>
#include <vnl/vnl_diag_matrix.h>
#include <vnl/vnl_vector.h>
#include <vnl/algo/vnl_matrix_inverse.h>
#include <vnl/vnl_sparse_matrix.h>

/** Third-order Tensor Class
 *
 *  A thorough and detailed introduction into tensors can be found in
 *
 * 	Tamara G Kolda and Brett W Bader. Tensor decompositions and
 * 	applications. SIAM review, 51(3):455–500, 2009.
 *
 *  It is highly recommended to read this review before starting
 *  to use this class. 	
 *
 *
 *  Dependencies:
 *  - The vnl matrix library is used. The class is tested with the ITK vnl libraries.
*/

class Tensor{
public:
	typedef double* 		iterator;
	typedef const double* 	const_iterator;

	typedef vnl_matrix<double> 			Matrix;
	typedef vnl_diag_matrix<double> 	DiagMatrix;
	typedef vnl_matrix_inverse<double> 	InverseMatrix;
	typedef vnl_sparse_matrix<double> 	SparseMatrix;
	typedef vnl_vector<double> 			Vector;

	typedef std::auto_ptr<Matrix> 		MatrixPointer;
	typedef std::auto_ptr<SparseMatrix> 	SparseMatrixPointer;
	typedef std::auto_ptr<Tensor> 		TensorPointer;
	typedef std::auto_ptr<Vector>		VectorPointer;

	// Constructor which allocates the memory and fills the tensor with zero values
	Tensor(unsigned c, unsigned r, unsigned t): columns(c), rows(r), tubes(t){
		// allocate memory
		data = new double[columns*rows*tubes];
		fill(0.0);
	}

	// Constructor which allocates the memroy and fills the tensor with the given data block
	Tensor(const std::vector<double>& data_block, unsigned c, unsigned r, unsigned t): columns(c), rows(r), tubes(t){
		assert(data_block.size() == c*r*t);
		data = new double[columns*rows*tubes];

		std::vector<double>::const_iterator it_data = data_block.begin();
		for(iterator it = this->begin(); it!=this->end(); it++){
			*it = *it_data;
			it_data++;
		}
	}

	// Destructor
	~Tensor(){
		// deallocate memory
		delete [] data;
	}

	// fills tensor with a constant value
	void fill(double value){
		for(iterator it = this->begin(); it!=this->end(); it++)
			*it = value;
	}

	// fills tensor with an increasing value
	void fill_numbering(){
		unsigned i=0;
		for(iterator it = this->begin(); it!=this->end(); it++){
			*it = i;
			i++;
		}
	}

	// some important helper functions
	iterator begin()const{return data;}
	iterator end()const{return data+this->size();}
	unsigned num_columns()const{return columns;}
	unsigned num_rows()const{return rows;}
	unsigned num_tubes()const{return tubes;}

	unsigned size()const{return columns*rows*tubes;}
	void set_size(unsigned c, unsigned r, unsigned t);
	void set_size(unsigned c, unsigned r, unsigned t, double value);
	double sse(const TensorPointer T)const;

	// Folding and unfolding is required for storing/loading tensors and for the decompositions.
	// See details in Kolda et al.
	MatrixPointer unfolding(unsigned mode)const;
	static TensorPointer folding(Matrix &mrx, unsigned mode, unsigned c, unsigned r, unsigned t);
	static TensorPointer GetTensor(std::vector< Matrix >* factor_matrices, Vector& lambdas); // returns Kruskal tensor
	static TensorPointer GetTensor(std::vector< Matrix >* factor_matrices, Matrix& core); // returns Tucker tensor

	// Higher order SVD of De Lathauwer et al.
	std::vector< Matrix > * HOSVD(Matrix& core, unsigned r)const;

	// Candecomp / Parafac decomposition of Kolda et al.
	std::vector< Matrix > * CPALS(Vector& lambdas, unsigned r, unsigned num_iter=150, double tol=1e-10, bool print_iter=false)const;

	// Some required matrix products
	static MatrixPointer kron(Matrix &A, Matrix &B);	// Kronecker product.
	static MatrixPointer krb(Matrix &A, Matrix &B);	// Khatri-Rao product.
	static MatrixPointer hm(Matrix &A, Matrix &B);	// Hadamard product (element-wise).

	// Alternating least squares method of Harshman
	static Matrix als(Matrix &X, Matrix &A, Matrix &B, bool normalize=true);

	static SparseMatrixPointer GetSymmetricSparseMatrix(const Matrix &A);
	static MatrixPointer GetSymmetricMatrix(const Matrix &A);

	static VectorPointer Matrix2Vector(const Matrix &A);
	static MatrixPointer Vector2Matrix(const Vector &A, unsigned c, unsigned r);

	void print(){
		std::cout.precision(5);
		iterator it = this->begin();
		for(unsigned i=0; i<tubes; i++){
			for(unsigned j=0; j<rows; j++){
				for(unsigned k=0; k<columns; k++){
					std::cout << *it << " ";
					it++;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl << std::endl;
		}
	}

	void print(unsigned mode){
		this->unfolding(mode)->print(std::cout);
	}

	void print_flat(){
		std::cout.precision(5);
		iterator it = this->begin();
		for(unsigned i=0; i<tubes; i++){
			for(unsigned j=0; j<rows; j++){
				for(unsigned k=0; k<columns; k++){
					std::cout << *it << " ";
					it++;
				}

			}
		}
		std::cout << std::endl;
	}

private:
	double *data;
	unsigned columns, rows, tubes;

	Tensor(const Tensor&); 			// implement this if you need it
	void operator=(const Tensor&);  // implement this if you need it
};


#endif
