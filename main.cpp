/*
   main.cpp
 
   Created on: Jun 22, 2011
       Author: Christoph Jud
        Email: christoph.jud@unibas.ch


Copyright (c) 2011, Christoph Jud
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include <iostream>
#include <string>

#include <sys/time.h>
#include <stdlib.h>

#include <vnl/vnl_matrix.h>
#include <vnl/algo/vnl_svd.h>

#include "TensorIO.h"
#include "Tensor.h"

// generates a random rxc matrix
vnl_matrix<double> rand_matrix(unsigned r, unsigned c){
	vnl_matrix<double> m(r,c);
	srand ( time(NULL) );
	for(unsigned i=0; i<r; i++)
		for(unsigned j=0; j<c; j++)
			m(i,j) = rand();

	return m;
}

// generates a matrix with increasing values
vnl_matrix<double> increasing_matrix(unsigned r, unsigned c){
	vnl_matrix<double> m(r,c);
	unsigned couter = 0;
	for(unsigned i=0; i<r; i++)
		for(unsigned j=0; j<c; j++)
			m(i,j) = couter++;

	return m;
}

// Test application which reads decomposes and writes a tensor
int main(int argc, char *argv[]){

	struct timeval start, end;

	if(argc != 3){
		std::cout << "Usage: " << argv[0] << " input_tensor.mat output_tensor.mat" << std::endl;
		return 0;
	}
	std::string input_filename = argv[1];
	std::string output_filename = argv[2];

	// read tensor
	Tensor::Matrix F = read_tensor(input_filename.c_str());


        unsigned columns = 31;
        unsigned rows = 31;
        unsigned tubes = 31;

        unsigned mode = 1;

        unsigned rank = 5;

        gettimeofday(&start, NULL);
        Tensor::TensorPointer T = Tensor::folding(F,mode,columns,rows,tubes);
        gettimeofday(&end, NULL);
        std::cout << "Tensor folding performed in: " << (end.tv_usec - start.tv_usec)/1000.0 << " milliseconds." << std::endl;

        Tensor::Matrix core;
        gettimeofday(&start, NULL);
        std::vector< Tensor::Matrix >* factor_matrices = T->HOSVD(core, rank);
        gettimeofday(&end, NULL);
        std::cout << "HOSVD performed in: " << (end.tv_usec - start.tv_usec)/1000.0 << " milliseconds." << std::endl;

        Tensor::TensorPointer A = Tensor::GetTensor(factor_matrices, core);
        factor_matrices->clear();


//        std::cout << "Matrix before" << std::endl;
//        std::cout << core << std::endl;
//        Tensor::VectorPointer v = Tensor::Matrix2Vector(core);
//        Tensor::MatrixPointer m = Tensor::Vector2Matrix(*v, core.columns(), core.rows());
//        std::cout << "Matrix after" << std::endl;
//        std::cout << *m << std::endl;


        Tensor::Vector lambdas;
        gettimeofday(&start, NULL);
        factor_matrices = T->CPALS(lambdas,rank);
        gettimeofday(&end, NULL);
        std::cout << "CPALS performed in: " << ((double)end.tv_usec - (double)start.tv_usec)/1000.0 << " milliseconds." << std::endl;


        std::cout << lambdas << std::endl;
        std::cout << "---------------------" << std::endl;
        for(std::vector< Tensor::Matrix >::iterator it=factor_matrices->begin(); it!=factor_matrices->end(); it++){
        	std::cout << (*it).get_column(0) << std::endl;
        	std::cout << "---------------------" << std::endl;
        }


        gettimeofday(&start, NULL);
        Tensor::TensorPointer B = Tensor::GetTensor(factor_matrices, lambdas);
        gettimeofday(&end, NULL);
        std::cout << "Tensor constructed in: " << (end.tv_usec - start.tv_usec)/1000.0 << " milliseconds." << std::endl;

        gettimeofday(&start, NULL);
        Tensor::MatrixPointer F_approx = B->unfolding(1);
        gettimeofday(&end, NULL);
        std::cout << "Tensor unfolding performed in: " << (end.tv_usec - start.tv_usec)/1000.0 << " milliseconds." << std::endl;



        // write tensor
        write_tensor(output_filename.c_str(), *F_approx);
        return 0;
}
